package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

  @Override
  public T findTriplicate(List<T> list) {
    for (int i = 0; i < list.size(); i++) {
      int countedNum = Collections.frequency(list, list.get(i));
      if (countedNum >= 3) {
        return list.get(i);
      }
    }
    return null;
  }
}
